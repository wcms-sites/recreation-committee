core = 7.x
api = 2

; uw_ct_exchange_board
projects[uw_ct_exchange_board][type] = "module"
projects[uw_ct_exchange_board][download][type] = "git"
projects[uw_ct_exchange_board][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_exchange_board.git"
projects[uw_ct_exchange_board][download][tag] = "7.x-1.5"
projects[uw_ct_exchange_board][subdir] = ""


